// JavaScript Document
$(document).ready(function(e) {
    $('#fac').change(function(){
		fac = $(this).val();
		dept = $('#dep');
		$.ajax({
			type: "GET",
			url: "courses.php",
			data: 'fac='+fac,
			success: function(response) {
				dept.html(response);
			}
		});
	});
	$('#dep').change(function() {
		dept = $(this).val();
		prog = $('#prog');
		$.ajax({
			type: "GET",
			url: "courses.php",
			data: 'dept='+dept,
			success: function(response) {
				prog.html(response);
			}
		});
	});
	$('#drop_list').validate({
		rules: {
			fac: "required",
			dep: "required",
		},
		messages: {
			fac: "Choose a faculty"
		}
	});
});
