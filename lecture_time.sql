-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 14, 2020 at 03:05 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lecture_time`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `username` varchar(60) NOT NULL,
  `password` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `username`, `password`) VALUES
(1, 'admin', 'c33367701511b4f6020ec61ded352059');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `c_code` varchar(10) NOT NULL,
  `semester` int(11) NOT NULL,
  `prog_id` int(11) NOT NULL,
  `course_title` varchar(100) NOT NULL,
  `credit_unit` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `course_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`c_code`, `semester`, `prog_id`, `course_title`, `credit_unit`, `level`, `course_id`) VALUES
('MAT 101', 1, 0, 'Elementary Mathematics', 3, 1, 10),
('MAT 102', 1, 1, 'Elementary Mathematics II', 3, 1, 11),
('MAT 203', 2, 1, 'Algebra', 3, 2, 12),
('MAT 103', 1, 1, 'Elementary Mathematics III', 3, 1, 13),
('MAT419', 2, 1, 'Graph Theory', 3, 1, 14),
('BIO101', 1, 4, 'Introduction to Biology', 3, 1, 15),
('PHY102', 2, 16, 'General Physics II', 3, 1, 16),
('PHY101', 1, 16, 'General Physics', 3, 1, 17),
('CMP 101', 1, 18, 'Introduction to Computer Science', 3, 1, 18),
('CMP201', 1, 18, 'Computer Programming I', 3, 1, 19),
('CMP 202', 1, 18, 'Computer Programming II', 3, 2, 20),
('GST 101', 1, 0, 'Communication in English', 2, 1, 21),
('PHY107', 1, 16, 'Laboratory', 3, 1, 22),
('GST 102', 1, 0, 'Nigerian People and Culture', 3, 1, 23),
('GST 103', 2, 0, 'Information and Communication', 3, 1, 24),
('CHM 101', 1, 20, 'General Chemistry', 3, 1, 25),
('MAT405', 1, 1, 'General Chemistry', 3, 1, 26);

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `dept_id` int(11) NOT NULL,
  `department` varchar(60) NOT NULL,
  `faculty_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`dept_id`, `department`, `faculty_id`) VALUES
(2, 'Chemistry', 0),
(3, 'Biochemistry', 0),
(4, 'Mathematics', 1),
(5, 'Microbiology', 0),
(6, 'Physics', 0),
(9, 'Biochemistry', 0),
(12, 'Biology', 1),
(13, 'Computer science', 1),
(14, 'Statistics', 1),
(15, 'Adult Education', 2),
(16, 'Fisheries', 3),
(17, 'Physics', 1),
(18, 'Biochemistry', 1),
(19, 'Microbiology', 1),
(21, 'Law', 4),
(22, 'Law', 5),
(23, 'Forestry', 3),
(24, 'Agriculture', 3),
(25, 'Hausa', 5),
(26, 'French', 5),
(27, 'Geography', 6),
(28, 'Economics', 6),
(29, 'Business Admin', 7),
(30, 'Political Sciences', 7),
(31, 'Accounting', 6),
(32, 'General Studies', 8),
(33, 'Chemistry', 1),
(34, 'French', 1);

-- --------------------------------------------------------

--
-- Table structure for table `exam`
--

CREATE TABLE `exam` (
  `exam_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `exam_time` varchar(11) NOT NULL,
  `c_code` varchar(10) NOT NULL,
  `mday` varchar(10) NOT NULL,
  `mdate` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam`
--

INSERT INTO `exam` (`exam_id`, `venue_id`, `exam_time`, `c_code`, `mday`, `mdate`) VALUES
(1, 25, '08:30-11:00', 'CMP 101', 'Monday', '14/01/2020'),
(2, 25, '12:00-02:00', 'CMP201', 'Monday', '14/01/2020'),
(3, 25, '03:00-05:00', 'MAT 101', 'Monday', '14/01/2020'),
(4, 26, '08:30-11:00', 'MAT 102', 'Tuesday', '15/01/2020'),
(5, 26, '12:00-02:00', 'MAT 103', 'Tuesday', '15/01/2020'),
(6, 26, '03:00-05:00', 'PHY101', 'Tuesday', '15/01/2020'),
(7, 27, '08:30-11:00', 'PHY102', 'Wednesday', '16/01/2020'),
(8, 27, '12:00-02:00', 'PHY107', 'Wednesday', '16/01/2020'),
(9, 27, '08:30-11:00', 'MAT 203', 'Monday', '14/01/2020'),
(10, 27, '12:00-02:00', 'MAT419', 'Monday', '14/01/2020');

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `faculty_id` int(11) NOT NULL,
  `faculty` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`faculty_id`, `faculty`) VALUES
(1, 'Science'),
(2, 'Education'),
(3, 'Agriculture'),
(5, 'Arts and Islamic Studies'),
(6, 'Social Sciences'),
(7, 'Management Science'),
(8, 'Division of General studies');

-- --------------------------------------------------------

--
-- Table structure for table `programs`
--

CREATE TABLE `programs` (
  `program_id` int(11) NOT NULL,
  `program_name` varchar(60) NOT NULL,
  `faculty_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `duration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `programs`
--

INSERT INTO `programs` (`program_id`, `program_name`, `faculty_id`, `dept_id`, `duration`) VALUES
(1, 'Bsc.Mathematics', 1, 4, 4),
(3, 'Bsc.Statistics', 1, 4, 4),
(4, 'Bsc. Biological Sciences', 1, 12, 4),
(6, 'Bsc. Fisheries', 3, 16, 5),
(8, 'Bsc. Geography', 6, 27, 4),
(9, 'Bsc. Microbiology', 1, 5, 4),
(10, 'Bsc. Agriculture', 3, 16, 4),
(11, 'B.A. Hausa', 5, 25, 4),
(12, 'Bsc. Economics', 6, 28, 4),
(13, 'Bsc. Accounting', 6, 31, 4),
(14, 'Bsc. Political Science', 7, 30, 4),
(15, 'Bsc. Business Administration', 7, 29, 4),
(16, 'Bsc. Physics', 1, 17, 4),
(17, 'Bsc. Biochemistry', 1, 18, 4),
(18, 'Bsc. Computer Science', 1, 13, 4),
(19, 'Bsc. Statistics', 1, 14, 4),
(20, 'Bsc. Chemistry', 1, 33, 4),
(21, 'B.A. French', 1, 34, 4);

-- --------------------------------------------------------

--
-- Table structure for table `reg_courses`
--

CREATE TABLE `reg_courses` (
  `adm_no` varchar(12) NOT NULL,
  `c_code` varchar(10) NOT NULL,
  `c_level` int(11) NOT NULL,
  `c_session` varchar(10) NOT NULL,
  `considered` enum('true','false','cant') NOT NULL DEFAULT 'false'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reg_courses`
--

INSERT INTO `reg_courses` (`adm_no`, `c_code`, `c_level`, `c_session`, `considered`) VALUES
('1210306004', 'MAT 101', 1, '2017/2018', 'true'),
('1210306004', 'MAT 102', 1, '2017/2018', 'true'),
('1210306004', 'MAT 203', 2, '2017/2018', 'true'),
('1210306004', 'MAT419', 1, '2017/2018', 'true'),
('1510310032', 'CMP 101', 1, '2018/2019', 'true'),
('1510310032', 'CMP201', 1, '2018/2019', 'true'),
('1510310032', 'MAT 101', 1, '2018/2019', 'true'),
('1510310032', 'MAT 102', 1, '2018/2019', 'true'),
('1510310032', 'MAT 103', 1, '2018/2019', 'true'),
('1510310032', 'PHY101', 1, '2018/2019', 'true'),
('1510310032', 'PHY102', 1, '2018/2019', 'true'),
('1510310032', 'PHY107', 1, '2018/2019', 'true');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `student_id` int(11) NOT NULL,
  `matric_no` varchar(15) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `surname` varchar(100) NOT NULL,
  `othername` varchar(100) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `prog_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`student_id`, `matric_no`, `firstname`, `surname`, `othername`, `phone`, `prog_id`, `level`, `password`) VALUES
(1, '1210306005', 'Mahmud', 'Bakale', 'Abdullahi', '08038969767', 1, 1, 'e10adc3949ba59abbe56e057f20f883e'),
(3, '1510310032', 'Sadiya', 'Bello', 'Anka', '08167882107', 18, 1, 'd0f1cfe86a3377cd9aa0577701fd892d'),
(4, '1510305019', 'Zahra', 'Muhammad', '', '08012345678', 16, 1, 'c42c7a82041901c72b656a0d0e122400'),
(5, '1510107005', 'Haleema', 'Zubairu', '', '09045667789', 21, 4, 'b9a7dac5f1fd47da5d7294964f71aa62'),
(8, '1510303085', 'Maryam', 'Sabiu', 'Liadi', '08076543214', 0, 4, 'd40f656f550b3da5707a5d44a16e5aca'),
(9, '1510303085', 'Maryam', 'Sabiu', 'Liadi', '08076543214', 0, 4, 'd40f656f550b3da5707a5d44a16e5aca'),
(10, '1510303085', 'Maryam', 'Sabiu', 'Liadi', '09076543215', 0, 1, 'd40f656f550b3da5707a5d44a16e5aca'),
(12, '1510308080', 'Zeenat', 'Bello', 'Abdullahi', '08067556655', 16, 2, 'ef9611841ff2f66d5d6fa04b1fe73a8f');

-- --------------------------------------------------------

--
-- Table structure for table `venue`
--

CREATE TABLE `venue` (
  `venue_id` int(11) NOT NULL,
  `venue_title` text NOT NULL,
  `venue_capacity` int(11) NOT NULL,
  `exam_capacity` int(11) NOT NULL,
  `full` enum('true','false') NOT NULL DEFAULT 'false'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `venue`
--

INSERT INTO `venue` (`venue_id`, `venue_title`, `venue_capacity`, `exam_capacity`, `full`) VALUES
(25, 'Multipurpose Hall', 200, 100, 'true'),
(26, 'Petroleum Trust Fund hall', 100, 70, 'true'),
(27, 'New Lecture Theatre', 100, 70, 'false'),
(29, 'Exam Hall', 100, 70, 'false');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`course_id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`dept_id`);

--
-- Indexes for table `exam`
--
ALTER TABLE `exam`
  ADD PRIMARY KEY (`exam_id`);

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`faculty_id`);

--
-- Indexes for table `programs`
--
ALTER TABLE `programs`
  ADD PRIMARY KEY (`program_id`);

--
-- Indexes for table `reg_courses`
--
ALTER TABLE `reg_courses`
  ADD PRIMARY KEY (`adm_no`,`c_code`,`c_session`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`student_id`);

--
-- Indexes for table `venue`
--
ALTER TABLE `venue`
  ADD PRIMARY KEY (`venue_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `course_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `dept_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `exam`
--
ALTER TABLE `exam`
  MODIFY `exam_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `faculty_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `programs`
--
ALTER TABLE `programs`
  MODIFY `program_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `student_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `venue`
--
ALTER TABLE `venue`
  MODIFY `venue_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
