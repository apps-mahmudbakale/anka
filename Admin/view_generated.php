<?php 
session_start();
include '../inc/connect.php';
include '../inc/class.validation.php';
include '../inc/function.php';

    if (isset($_GET['id'])) {
        mysqli_query($connection,"DELETE FROM courses WHERE course_id='$_GET[id]'");
    }
 ?>
 
<!DOCTYPE html>
<html>
<head>
	 <title>Time Table Generating System</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="shortcut icon" href="../images/udus-logo.png" />
  <link rel="stylesheet" type="text/css" href="../css/screen.css">
  <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../datatables.net-bs/css/dataTables.bootstrap.min.css">
</head>
<body>
<p></p>
<p></p>
<br>
<br>
<div id="container">
  <div class="row " style="margin-top: 70px;">
    
     <ul class="nav nav-pills pull-right">
        <li><a href="index.php" style="text-decoration: none;"><i class="fa fa-home"></i> Home</a></li>
         <li><a href="departments.php" style="text-decoration: none;"><i class="fa fa-building"></i> Departments</a></li>
        <li class="dropdown">
              <a href="#" class="dropdown-toggle" style="text-decoration: none;" data-toggle="dropdown"><i class="fa fa-cogs"></i> Time Table <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="courses.php" style="text-decoration: none;"><i class="fa fa-book"></i> Courses</a></li>
                <li><a href="venues.php" style="text-decoration: none;"><i class="fa fa-building"></i> Venues</a></li>
                <li><a href="#" style="text-decoration: none;"><i class="fa fa-table"></i> Time Table</a></li>
              </ul>
            </li>
        <li><a href="students.php" style="text-decoration: none;"><i class="fa fa-graduation-cap"></i>View Students</a></li>
        <li><a href="view_result.php" style="text-decoration: none;"><i class="fa fa-edit"></i>Change Password</a></li>
        <li><a href="../logout.php" style="text-decoration: none;"><i class="fa fa-power-off"></i>Logout</a></li>
      </ul>
		<div class="col-lg-11 col-md-11 well" style="margin-left: 6px; min-height: 590px; width:98.666668% ;">
           <fieldset>
             <legend><i class="fa fa-eye"></i> View Generated Time Table</legend>
				      <form action="" method="POST" role=form>
              <div class="col-lg-6">
                Semester:
                <select name="semester" class="form-control">
                  <option value="1">First Semester</option>
                  <option value="2"> Second Semester</option>
                </select>
              </div>

              <div class="col-lg-2">
                <br>
                <input type="submit" name="submit" class="btn btn-success" value="View Timetable">
              </div>  
              </form>
           </fieldset>
            <hr>
           <p></p>
           <?php
if(isset($_POST['submit'])){
  $semester=trim($_POST['semester']);
  $sem="for".$semester;
$select=mysqli_query($db,"SELECT * FROM generated WHERE semester='".$semester."'") or die(mysql_error());
$nums=mysqli_num_rows($select);

if($nums==0){
  echo '<br/><br/><p style="width:50%;margin:auto;text-align:center;font-size:18px;color:#F00;"> NO TIMETABLE HAS BEEN GENERATED</p>';
  }
else{
  $fetch=mysqli_fetch_array($select);
  print_r($fetch);
  $venues=explode("||",$fetch[2]);
  $cells=explode("||",$fetch[3]);
  $num=count($venues);
  



 echo'<table width="60%" border="1" style="border-collapse:collapse; margin:auto" class="table table-striped table-bordered table-hover table-condensed table-responsive">
     <tbody>
    <tr>
    <th scope="col" width="20%">&nbsp;DATES</th>
      <th scope="col" width="8%">&nbsp;VENUES</th>
      <th scope="col" width="10%">&nbsp;8-10</th>
      <th scope="col" width="10%">&nbsp;10-12</th>
      <th scope="col" width="10%">&nbsp;12-2</th>
      <th scope="col" width="10%">&nbsp;2-4</th>
      <th scope="col" width="10%">&nbsp;4-6</th>
    </tr>';
$days=array("MONDAY","TUESDAY","WEDNESDAY","THURSDAY","FRIDAY");
$h=0;

$n=0;
  while($n<count($days)){
    
  if($days[$n]=="FRIDAY"){
    $nn=1;
  echo'
    <tr>
      <th scope="row" rowspan='.$num.'>&nbsp;'.$days[$n].'</th>
      <td style="font-weight:bold;">&nbsp;'.$venues[0].'</td>
      <td style="background-color:#DEDEDE;">&nbsp;</td>
      <td style="background-color:#DEDEDE;">&nbsp;</td>
      <td style="background-color:#DEDEDE;">&nbsp;</td>
    <td style="background-color:#DEDEDE;">&nbsp;</td>
    <td>&nbsp;'.$cells[$h].'</td>
    </tr>';
  $h=$h+1;
  $start=1;
  while($nn<$num){
  echo'
  <tr>
    <td style="font-weight:bold;">&nbsp;'.$venues[$start].'</td>
      <td style="background-color:#DEDEDE;">&nbsp;</td>
      <td style="background-color:#DEDEDE;">&nbsp;</td>
      <td style="background-color:#DEDEDE;">&nbsp;</td>
    <td style="background-color:#DEDEDE;">&nbsp;</td>
    <td>&nbsp;'.$cells[$h].'</td>
   </tr>';
  $h=$h+1;
  $start++;
  $nn++;}}
  else{
  $nn=1;
  echo'
    <tr>
      <th scope="row" rowspan='.$num.'>&nbsp;'.$days[$n].'</th>
      <td style="font-weight:bold;">&nbsp;'.$venues[0].'</td>
      <td>&nbsp;'.$cells[$h].'</td>
      <td>&nbsp;'.$cells[$h+1].'</td>
      <td>&nbsp;'.$cells[$h+2].'</td>
    <td>&nbsp;'.$cells[$h+3].'</td>
    <td>&nbsp;'.$cells[$h+4].'</td>
    </tr>';
  $h=$h+5;
  $start=1;
  while($nn<$num){
  echo'
  <tr>
    <td style="font-weight:bold;">&nbsp;'.$venues[$start].'</td>
    <td>&nbsp;'.$cells[$h].'</td>
      <td>&nbsp;'.$cells[$h+1].'</td>
      <td>&nbsp;'.$cells[$h+2].'</td>
      <td>&nbsp;'.$cells[$h+3].'</td>
    <td>&nbsp;'.$cells[$h+4].'</td>
   </tr>';
  $h=$h+5;
  $start++;
  $nn++;}}
  $n++;}
  
  echo'</tbody>
</table><br/><br/>';}}
?>
       </div>
	</div>
</div>
<script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
<script src="../bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../datatables/jquery.dataTables.min.js"></script>
<script src="../datatables/dataTables.bootstrap.min.js"></script>
<script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>


</body>
</html>