<?php 
session_start();
include '../inc/connect.php';
include '../inc/class.validation.php';
include '../inc/function.php';

    if (isset($_GET['id'])) {
        mysqli_query($connection,"DELETE FROM courses WHERE course_id='$_GET[id]'");
    }
 ?>

<!DOCTYPE html>
<html>
<head>
   <title>Time Table Generating System</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="shortcut icon" href="../images/udus-logo.png" />
  <link rel="stylesheet" type="text/css" href="../css/screen.css">
  <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../datatables.net-bs/css/dataTables.bootstrap.min.css">
</head>
<body>
<p></p>
<p></p>
<br>
<br>
<div id="container">
  <div class="row " style="margin-top: 70px;">
    
     <ul class="nav nav-pills pull-right">
        <li><a href="index.php" style="text-decoration: none;"><i class="fa fa-home"></i> Home</a></li>
         <li><a href="departments.php" style="text-decoration: none;"><i class="fa fa-building"></i> Departments</a></li>
        <li class="dropdown">
              <a href="#" class="dropdown-toggle" style="text-decoration: none;" data-toggle="dropdown"><i class="fa fa-cogs"></i> Time Table <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="courses.php" style="text-decoration: none;"><i class="fa fa-book"></i> Courses</a></li>
                <li><a href="venues.php" style="text-decoration: none;"><i class="fa fa-building"></i> Venues</a></li>
                <li><a href="timetable.php" style="text-decoration: none;"><i class="fa fa-table"></i> Time Table</a></li>
                 <li><a href="timetable.php" style="text-decoration: none;"><i class="fa fa-table"></i> View Generated Time Table</a></li>
              </ul>
            </li>
        <li><a href="students.php" style="text-decoration: none;"><i class="fa fa-graduation-cap"></i>View Students</a></li>
        <li><a href="view_result.php" style="text-decoration: none;"><i class="fa fa-edit"></i>Change Password</a></li>
        <li><a href="../logout.php" style="text-decoration: none;"><i class="fa fa-power-off"></i>Logout</a></li>
      </ul>
		<div class="col-lg-11 col-md-11 well" style="margin-left: 6px; min-height: 590px; width:98.666668% ;">
    <?php 

        $id = $_GET['id'];
        $query2 = $db->query("SELECT * FROM programs WHERE program_id ='$id'");
        $row = mysqli_fetch_object($query2);

      if (isset($_POST['submit'])) {
        $prog = $_POST['prog'];
        $dept = $_POST['dept_id'];
        $faculty = $_POST['faculty_id'];
        $duration = $_POST['duration'];
        $query = $db->query("UPDATE `programs` SET `program_name`='$prog',`faculty_id`='$faculty',`dept_id`='$dept',`duration`='$duration' WHERE program_id = '$id'");

        if ($query) {
          echo '<script>alert("Program Successfully Updated"); window.location="programs.php";</script>';
        } else{
          echo Error('Error');
        }

      }
        $id = $_GET['id'];
        $query2 = $db->query("SELECT * FROM programs p INNER JOIN faculty f ON p.faculty_id = f.faculty_id INNER JOIN department d ON p.dept_id = d.dept_id WHERE p.program_id ='$id'");
        $row = mysqli_fetch_array($query2);
    ?>
           <fieldset>
             <legend><i class="fa fa-edit"></i> Edit Program</legend>
			         <form action="" method="POST" role=form>
              <div class="col-lg-6">
                  Program Name:
                <input type="text" name="prog" value="<?php echo $row['program_name']; ?>" class="form-control">
              </div> 
              <div class="col-lg-6">
                Faculty
                <select name="faculty_id" id="faculty_id" class="form-control">
                  <option selected value="<?php echo $row['faculty_id'] ?>"><?php echo $row['faculty'] ?></option>
                  <?php 
                  $query = mysqli_query($db,"SELECT * FROM faculty");

                  while ($rows = mysqli_fetch_array($query)) {
                    echo "<option value='".$rows['faculty_id']."'>".$rows['faculty']."</option>";
                  }

                   ?>
                </select>
              </div>
              <div class="col-lg-6">
                Department
                <select name="dept_id"  id="dept_id" class="form-control">
                  <option selected value="<?php echo $row['dept_id'] ?>"><?php echo $row['department'] ?></option>
                  
                </select>
              </div>
                <div class="col-lg-6">
                Duration
               <input name="duration" value="<?php echo $row['duration'] ?>" type="text" class="form-control">
              </div>
              <div class="col-lg-2">
                <br>
                <input type="submit" name="submit" class="btn btn-success" value="Update">
              </div>  
              </form>
              </form>
           </fieldset>
       </div>
       
	</div>
</div>
<script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
<script src="../bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../datatables/jquery.dataTables.min.js"></script>
<script src="../datatables/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(e){
  $('#faculty_id').change(function(e) {
        $.ajax({
            type:'GET',
            url:'ajaxdept.php',
            data: 'faculty_id='+$(this).val(),
            success:function(r) {
                $('#dept_id').html(r);
                //alert(r);
            }
        })
   })
  
});
</script>
</body>
</html>