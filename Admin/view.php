<?php 
session_start();
include '../inc/connect.php';
include '../inc/class.validation.php';
include '../inc/function.php';
 ?>
 
<!DOCTYPE html>
<html>
<head>
   <title>Time Table Generating System</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="shortcut icon" href="../images/udus-logo.png" />
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../datatables.net-bs/css/dataTables.bootstrap.min.css">
</head>
<body>    
<div class="container">
  <div class='col-lg-12'>
            <div class="col-lg-1"><img src="../images/udus-logo.png"/></div>
              <div class="col-lg-10" style="text-align:center"><h2>USMANU DANFODIO UNIVERSITY SOKOTO - NIGERIA</h2>
                        <h3>EXAMINATION TIME TABLE</h3>
                </div>
            </div>
            <br>
  <div class="row">
    <div class="col-12">
      <div class="row day-columns">
      <?php $days = mysqli_query($db,"SELECT DISTINCT mdate, mday FROM exam");
      $styles = array('gray','red','blue','navy','purple','orange');
      while ($day = mysqli_fetch_array($days)) {
      ?>
        <div class="day-column">
          <div class="day-header"><?php echo $day['mday'] ?> <?php echo $day['mdate'] ?></div>
          <div class="day-content">
            <?php  
              $query = mysqli_query($db,"SELECT * FROM exam e INNER JOIN venue v ON e.venue_id = v.venue_id WHERE mday='".$day['mday']."'");
              while ($row = mysqli_fetch_array($query)) {
                $style = array_random($styles);
            ?>
            <div class="event <?php echo $style ?>">
                <span class="title"><?php echo $row['c_code'] ?></span>
                <footer>
                  <span><?php echo $row['venue_title']?></span>
                  <span><?php echo $row['exam_time'] ?></span>
                </footer>
            </div>
          <?php } ?>
          </div>
        </div>
        <?php 
          }
         ?>
      </div>
    </div>
  </div>
  <br>
  <button class="btn btn-primary btn-block" onclick="window.print();"><i class="fa fa-print"></i> Print Time Table</button>
</div>
</body>
</html>