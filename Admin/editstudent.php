<?php 
session_start();
include '../inc/connect.php';
include '../inc/class.validation.php';
include '../inc/function.php';
 ?>
<!DOCTYPE html>
<html>
<head>
   <title>Time Table Generating System</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="shortcut icon" href="../images/udus-logo.png" />
  <link rel="stylesheet" type="text/css" href="../css/screen.css">
  <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../datatables.net-bs/css/dataTables.bootstrap.min.css">
</head>
<body>
<p></p>
<p></p>
<br>
<br>
<div id="container">
  <div class="row " style="margin-top: 70px;">
    
     <ul class="nav nav-pills pull-right">
        <li><a href="index.php" style="text-decoration: none;"><i class="fa fa-home"></i> Home</a></li>
         <li><a href="departments.php" style="text-decoration: none;"><i class="fa fa-building"></i> Departments</a></li>
        <li class="dropdown">
              <a href="#" class="dropdown-toggle" style="text-decoration: none;" data-toggle="dropdown"><i class="fa fa-cogs"></i> Time Table <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="courses.php" style="text-decoration: none;"><i class="fa fa-book"></i> Courses</a></li>
                <li><a href="venues.php" style="text-decoration: none;"><i class="fa fa-building"></i> Venues</a></li>
                <li><a href="timetable.php" style="text-decoration: none;"><i class="fa fa-table"></i> Time Table</a></li>
                 <li><a href="timetable.php" style="text-decoration: none;"><i class="fa fa-table"></i> View Generated Time Table</a></li>
              </ul>
            </li>
        <li><a href="students.php" style="text-decoration: none;"><i class="fa fa-graduation-cap"></i>View Students</a></li>
        <li><a href="view_result.php" style="text-decoration: none;"><i class="fa fa-edit"></i>Change Password</a></li>
        <li><a href="../logout.php" style="text-decoration: none;"><i class="fa fa-power-off"></i>Logout</a></li>
      </ul>
		<div class="col-lg-11 col-md-11 well" style="margin-left: 6px; min-height: 590px; width:98.666668% ;">

        <?php $edit = $_GET['id'];
            $query = "SELECT * FROM students s INNER JOIN programs d ON s.prog_id = d.program_id WHERE s.student_id='$edit' ";

            $result = $db->query($query);

            while($record = mysqli_fetch_array($result)){
              $id = $record['student_id'];

 
         ?>

                                    <form action="" method="POST">
                                      <input type="hidden" name="id" class="form-control"/  value="<?php echo $id; ?>">
                                        <div class="col-lg-6">
                                            Admission Number:
                                            <input type="text" name="number" class="form-control"/  value="<?php echo $record['matric_no']; ?>">
                                        </div>
                                        <div class="col-lg-6">
                                            Surname:
                                            <input type="text" name="surname"    class="form-control"/  value="<?php echo $record['surname']; ?>">
                                        </div>

                                        <div class="col-lg-6">
                                            First Name:
                                            <input type="text" name="firstname"  class="form-control"/ value="<?php echo $record['firstname']; ?>">
                                        </div>

                                        <div class="col-lg-6">
                                            Other Name:
                                            <input type="text" name='oname' class="form-control" value="<?php echo $record['othername']; ?>">
                                        </div>

                                      <div class="col-lg-6">
                                        Programs:
                                          <input type="hidden" name="prog_id" value="<?php echo $record['program_id'] ?>">
                                          <input type="text" readonly value="<?php echo $record['program_name'] ?>" class="form-control">
                                        </div>  

                                        <div class="col-lg-6">
                                        Phone:
                                         <input type="text" name="phone" class="form-control" value="<?php echo $record['phone']; ?>">
                                        </div>

                                        &nbsp;<br/>
                                        <div class="col-lg-6">
                                            <button type="submit" name="update" class="btn btn-success"><i class="fa fa-check-circle"></i> Update Student </button>
                                        </div> <br>
                        </form>

<?php 
        }
              if (isset($_POST['update'])) {
               $number = $_POST['number'];
               $surname = $_POST['surname'];
               $firstname = $_POST['firstname'];
               $othername = $_POST['oname'];
               $prog_id = $_POST['prog_id'];
               $phone = $_POST['phone'];

               $query1 = "UPDATE `students` SET `matric_no`='$number',`firstname`='$firstname',`surname`='$surname',`othername`='$othername',`phone`='$phone',`prog_id`='$prog_id' WHERE `student_id` = '$id'";
               $result = $db->query($query1);

               if ($result) {
                echo '<br>';
                 echo success('Record Updated Suceesfull');
               } else{
                echo error('Registration Failed');
               }
             }
        ?>
      </div>
  </div>
</div>
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript">
