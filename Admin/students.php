<?php 
session_start();
include '../inc/connect.php';
include '../inc/class.validation.php';
include '../inc/function.php';
if (isset($_GET['id'])) {
  mysqli_query($db, "DELETE FROM students WHERE student_id ='$_GET[id]'");
  echo '<script>alert("Student Deleted Successfully"); window.location="students.php";</script>';
}
 ?>

<!DOCTYPE html>
<html>
<head>
   <title>Time Table Generating System</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="shortcut icon" href="../images/udus-logo.png" />
  <link rel="stylesheet" type="text/css" href="../css/screen.css">
  <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../datatables.net-bs/css/dataTables.bootstrap.min.css">
</head>
<body>
<p></p>
<p></p>
<br>
<br>
<div id="container">
  <div class="row " style="margin-top: 70px;">
    
     <ul class="nav nav-pills pull-left">
        <li><a href="index.php" style="text-decoration: none;"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="changepass.php" style="text-decoration: none;"><i class="fa fa-edit"></i> Change Password</a></li>
        <li><a href="../logout.php" style="text-decoration: none;"><i class="fa fa-power-off"></i> Logout</a></li>
      </ul>
		<div class="col-lg-11 col-md-11 well" style="margin-left: 6px; min-height: 590px; width:98.666668% ;">
      <div class="col-lg-12">
        <div class="col-lg-3">
        <ul class="sidebar">
        <li><a href='faculty.php'> <i class="fa fa-th"></i> Manage Faculty</a></li>
        <li><a href='departments.php'> <i class="fa fa-building"></i> Manage Departments</a></li>
        <li><a href='programs.php'> <i class="fa fa-files-o"></i> Manage Programs</a></li>
        <li><a href='courses.php'> <i class="fa fa-book"></i> Manage Courses</a></li>
         <li><a href='venues.php'> <i class="fa fa-bank"></i> Manage Venues</a></li>
         <li><a href='students.php'> <i class="fa fa-graduation-cap"></i> Manage Students</a></li>
         <li><a href='timetable.php'> <i class="fa fa-random"></i> Generate Time Table</a></li>
         <li><a href='.php'> <i class="fa fa-table"></i> View Time Table</a></li>
         <li><a href='registration.php' target="blank"> <i class="fa fa-pencil"></i> View Courses</a></li>
         
        </ul>
        </div>
        <div class="col-lg-9">
          <fieldset>
             <legend><i class="fa fa-graduation-cap"></i> List Of Students</legend>

             <table class="table table-striped">
               <thead>
                 <tr>
                   <th>S/N</th>
                   <th>Admission Number</th>
                   <th>First Name</th>
                   <th>Surname</th>
                   <th>Other Name</th>
                   <th>Program</th>
                   <th>Phone</th>
                   <th>Operations</th>
                 </tr>
               </thead>
               <tbody>
                 <?php
                 $page = (isset($_GET['page'])) ? $_GET['page'] : 1;

                  $limit = 5;
                  $limit_start = ($page - 1) * $limit;
                 $query = $db->query("SELECT * FROM students s INNER JOIN programs d ON s.prog_id = d.program_id LIMIT ".$limit_start.",".$limit);
                  $no = $limit_start + 1;
                  $sn = 0;
                 while ($row = $query->fetch_array(MYSQLI_ASSOC)) {
                  $id = $row['student_id'];

                  $sn++;
                   echo '<tr>';
                   echo'<td>'.$sn.'</td>';
                   echo'<td>'.$row['matric_no'].'</td>';
                   echo'<td>'.$row['firstname'].'</td>';
                   echo'<td>'.$row['surname'].'</td>';
                   echo'<td>'.$row['othername'].'</td>';
                   echo'<td>'.$row['program_name'].'</td>';
                   echo'<td>'.$row['phone'].'</td>';
                   echo'<td><a href="editstudent.php?id='.$id.'"  class="btn btn-default"><i class="fa fa-edit"></i></a> <a href="?id= '.$id. '" class="btn btn-default"><i class="fa fa-trash-o"></i></a></td>';
                   echo '</tr>';
                   $no++;
                 }
                 ?>

               </tbody>

             </table>
              <ul class="pagination">
            <!-- LINK FIRST AND PREV -->
            <?php
            if ($page == 1) { // Jika page adalah pake ke 1, maka disable link PREV
            ?>
                <li class="disabled"><a href="#">First</a></li>
                <li class="disabled"><a href="#">&laquo;</a></li>
            <?php
            } else { // Jika buka page ke 1
                $link_prev = ($page > 1) ? $page - 1 : 1;
            ?>
                <li><a href="students.php?page=1">First</a></li>
                <li><a href="students.php?page=<?php echo $link_prev; ?>">&laquo;</a></li>
            <?php
            }
            ?>

            <!-- LINK NUMBER -->
            <?php
            // Buat query untuk menghitung semua jumlah data
            $sql2 = mysqli_query($db,"SELECT COUNT(*) AS student FROM students");
            $get_jumlah = mysqli_fetch_array($sql2);

            $jumlah_page = ceil($get_jumlah['student'] / $limit); // Hitung jumlah halamanya
            $jumlah_number = 3; // Tentukan jumlah link number sebelum dan sesudah page yang aktif
            $start_number = ($page > $jumlah_number) ? $page - $jumlah_number : 1; // Untuk awal link member
            $end_number = ($page < ($jumlah_page - $jumlah_number)) ? $page + $jumlah_number : $jumlah_page; // Untuk akhir link number

            for ($i = $start_number; $i <= $end_number; $i++) {
                $link_active = ($page == $i) ? 'class="active"' : '';
            ?>
                <li <?php echo $link_active; ?>><a href="students.php?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
            <?php
            }
            ?>

            <!-- LINK NEXT AND LAST -->
            <?php
            // Jika page sama dengan jumlah page, maka disable link NEXT nya
            // Artinya page tersebut adalah page terakhir
            if ($page == $jumlah_page) { // Jika page terakhir
            ?>
                <li class="disabled"><a href="#">&raquo;</a></li>
                <li class="disabled"><a href="#">Last</a></li>
            <?php
            } else { // Jika bukan page terakhir
                $link_next = ($page < $jumlah_page) ? $page + 1 : $jumlah_page;
            ?>
                <li><a href="students.php?page=<?php echo $link_next; ?>">&raquo;</a></li>
                <li><a href="students.php?page=<?php echo $jumlah_page; ?>">Last</a></li>
            <?php
            }
            ?>
        </ul>
           </fieldset>
        </div>
      </div>
   
           
       </div>
       
	</div>
</div>
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
</body>
</html>