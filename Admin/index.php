 <?php 
session_start();
include '../inc/connect.php';
include '../inc/class.validation.php';
include '../inc/function.php';


 ?>

<!DOCTYPE html>
<html>
<head>
	<title>Time Table Generating System</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="shortcut icon" href="../images/udus-logo.png" />
	<link rel="stylesheet" type="text/css" href="../css/screen.css">
  <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <script src="pgcourselist.js" type="text/javascript"> </script>
</head>
<body>
<p></p>
<p></p>
<br>
<br>
<div id="container">
	<div class="row " style="margin-top: 70px;">
    
    <ul class="nav nav-pills">
        <li><a href="index.php" style="text-decoration: none;"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="changepass.php" style="text-decoration: none;"><i class="fa fa-edit"></i>Change Password</a></li>
        <li><a href="../logout.php" style="text-decoration: none;"><i class="fa fa-power-off"></i>Logout</a></li>
      </ul>
		<div class="col-lg-11 col-md-11 well" style="margin-left: 6px; min-height: 590px; width:98.666668% ;">
      <div class="col-lg-12">
        <div class="col-lg-4">
        <br>
        <br>
        <br>
        <ul class="sidebar">
        <li><a href='faculty.php'> <i class="fa fa-th"></i> Manage Faculty</a></li>
        <li><a href='departments.php'> <i class="fa fa-building"></i> Manage Departments</a></li>
        <li><a href='programs.php'> <i class="fa fa-files-o"></i> Manage Programs</a></li>
        <li><a href='courses.php'> <i class="fa fa-book"></i> Manage Courses</a></li>
         <li><a href='venues.php'> <i class="fa fa-bank"></i> Manage Venues</a></li>
         <li><a href='students.php'> <i class="fa fa-graduation-cap"></i> Manage Students</a></li>
         <li><a href='timetable.php'> <i class="fa fa-random"></i> Generate Time Table</a></li>
         <li><a href='view.php'> <i class="fa fa-table"></i> View Time Table</a></li>
         <li><a href='registration.php' target="blank"> <i class="fa fa-pencil"></i> View Courses</a></li>

        </ul>
        </div>
          <div class="col-lg-8">
             <fieldset>
             <legend><h3>Welcome</h3></legend>
        <img src="../images/timetable.jpg" width="101%" />
           </fieldset>
          </div>
      </div>
          
       </div>
       
	</div>
</div>
<script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
<script src="../bootstrap/dist/js/bootstrap.min.js"></script>

</body>
</html>