<?php 
session_start();
include '../inc/connect.php';
include '../inc/class.validation.php';
include '../inc/function.php';

    if (isset($_GET['id'])) {
        mysqli_query($connection,"DELETE FROM courses WHERE course_id='$_GET[id]'");
    }
 ?>

<!DOCTYPE html>
<html>
<head>
   <title>Time Table Generating System</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="shortcut icon" href="../images/udus-logo.png" />
  <link rel="stylesheet" type="text/css" href="../css/screen.css">
  <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../datatables.net-bs/css/dataTables.bootstrap.min.css">
</head>
<body>
<p></p>
<p></p>
<br>
<br>
<div id="container">
  <div class="row " style="margin-top: 70px;">
    
     <ul class="nav nav-pills pull-right">
        <li><a href="index.php" style="text-decoration: none;"><i class="fa fa-home"></i> Home</a></li>
         <li><a href="departments.php" style="text-decoration: none;"><i class="fa fa-building"></i> Departments</a></li>
        <li class="dropdown">
              <a href="#" class="dropdown-toggle" style="text-decoration: none;" data-toggle="dropdown"><i class="fa fa-cogs"></i> Time Table <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="courses.php" style="text-decoration: none;"><i class="fa fa-book"></i> Courses</a></li>
                <li><a href="venues.php" style="text-decoration: none;"><i class="fa fa-building"></i> Venues</a></li>
                <li><a href="timetable.php" style="text-decoration: none;"><i class="fa fa-table"></i> Time Table</a></li>
                 <li><a href="timetable.php" style="text-decoration: none;"><i class="fa fa-table"></i> View Generated Time Table</a></li>
              </ul>
            </li>
        <li><a href="students.php" style="text-decoration: none;"><i class="fa fa-graduation-cap"></i>View Students</a></li>
        <li><a href="view_result.php" style="text-decoration: none;"><i class="fa fa-edit"></i>Change Password</a></li>
        <li><a href="../logout.php" style="text-decoration: none;"><i class="fa fa-power-off"></i>Logout</a></li>
      </ul>
		<div class="col-lg-11 col-md-11 well" style="margin-left: 6px; min-height: 590px; width:98.666668% ;">
    <?php 

        $id = $_GET['id'];
        $query2 = $db->query("SELECT * FROM courses WHERE course_id ='$id'");
        $row = mysqli_fetch_object($query2);

      if (isset($_POST['submit'])) {
        $cunit = $_POST['unit'];
        $code = $_POST['code'];
        $prog_id = $_POST['prog_id'];
        $semester = $_POST['semester'];
        $title  = $_POST['title'];

        $query = $db->query("UPDATE `courses` SET `c_code`='$code', `course_title` = '$title',  `credit_unit` = '$cunit', `semester`='$semester',`prog_id`='$prog_id' WHERE  `course_id` = '$id'");

        if ($query) {
          echo '<script>alert("Course Successfully Updated"); window.location="courses.php";</script>';
        } else{
          echo Error('Error');
        }

      }
        $id = $_GET['id'];
        $query2 = $db->query("SELECT * FROM courses c INNER JOIN programs d ON c.prog_id = d.program_id WHERE c.course_id ='$id'");
        $row = mysqli_fetch_array($query2);
    ?>
           <fieldset>
             <legend><i class="fa fa-edit"></i> Edit Course</legend>
				      <form action="" method="POST" role=form>
              <div class="col-lg-6">
              Course Code:
                <input type="text" name="code" value="<?php echo $row['c_code'];?>" class="form-control">
              </div>
              <div class="col-lg-6">
              Course Title:
                <input type="text" name="title" value="<?php echo $row['course_title'];?>" class="form-control">
              </div>
               <div class="col-lg-6">
              Credit Unit:
                <input type="text" name="unit" value="<?php echo $row['credit_unit'];?>" class="form-control">
              </div>
              <div class="col-lg-6">
              Program:
                <select name="prog_id" class="form-control">
                  <option value="<?php echo $row['program_id'] ?>" selected><?php echo $row['program_name'] ?></option>
                  <?php 
                  $query = mysqli_query($db,"SELECT * FROM programs WHERE dept_id ='$row[dept_id]'");
                  while ($rows = mysqli_fetch_array($query)){
                      echo "<option value='".$rows['program_id']."'>".$rows['program_name']."</option>";
                  }

                  ?>
               </select>
              </div>  
              <div class="col-lg-6">
                Semester:
                <select name="semester" class="form-control">
                  <option selected value="<?php echo $row['semester'] ?>">
                    <?php if ($row['semester'] == 1) {
                     echo "First Semester";
                    }elseif ($row['semester'] == 2) {
                      echo "Second Semester";
                    } ?>
                  </option>
                  <option value="1">First Semester</option>
                  <option value="2"> Second Semester</option>
                </select>
              </div>

              <div class="col-lg-2">
                <br>
                <input type="submit" name="submit" class="btn btn-success" value="Update Course">
              </div>  
              </form>
           </fieldset>
       </div>
       
	</div>
</div>
<script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
<script src="../bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../datatables/jquery.dataTables.min.js"></script>
<script src="../datatables/dataTables.bootstrap.min.js"></script>
<script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
</body>
</html>