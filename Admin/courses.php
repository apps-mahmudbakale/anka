<?php 
session_start();
include '../inc/connect.php';
include '../inc/class.validation.php';
include '../inc/function.php';

    if (isset($_GET['id'])) {
        mysqli_query($db,"DELETE FROM courses WHERE course_id='$_GET[id]'");
    }
 ?>

<!DOCTYPE html>
<html>
<head>
	 <title>Time Table Generating System</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="shortcut icon" href="../images/udus-logo.png" />
  <link rel="stylesheet" type="text/css" href="../css/screen.css">
  <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../datatables.net-bs/css/dataTables.bootstrap.min.css">
</head>
<body>
<p></p>
<p></p>
<br>
<br>
<div id="container">
  <div class="row " style="margin-top: 70px;">
    
     <ul class="nav nav-pills pull-left">
        <li><a href="index.php" style="text-decoration: none;"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="changepass.php" style="text-decoration: none;"><i class="fa fa-edit"></i>Change Password</a></li>
        <li><a href="../logout.php" style="text-decoration: none;"><i class="fa fa-power-off"></i>Logout</a></li>
      </ul>
		<div class="col-lg-11 col-md-11 well" style="margin-left: 6px; min-height: 590px; width:98.666668% ;">
      <div class="col-lg-12">
        <div class="col-lg-3">
       <ul class="sidebar">
        <li><a href='faculty.php'> <i class="fa fa-th"></i> Manage Faculty</a></li>
        <li><a href='departments.php'> <i class="fa fa-building"></i> Manage Departments</a></li>
        <li><a href='programs.php'> <i class="fa fa-files-o"></i> Manage Programs</a></li>
        <li><a href='courses.php'> <i class="fa fa-book"></i> Manage Courses</a></li>
         <li><a href='venues.php'> <i class="fa fa-bank"></i> Manage Venues</a></li>
         <li><a href='students.php'> <i class="fa fa-graduation-cap"></i> Manage Students</a></li>
         <li><a href='timetable.php'> <i class="fa fa-random"></i> Generate Time Table</a></li>
         <li><a href='.php'> <i class="fa fa-table"></i> View Time Table</a></li>
         <li><a href='registration.php' target="blank"> <i class="fa fa-pencil"></i> View Courses</a></li>

        </ul>
        </div>
        <div class="col-lg-9">
           <?php 


      if (isset($_POST['submit'])) {
        $dept = $_POST['dept_id'];
        $code = $_POST['code'];
        $semester = $_POST['semester'];
        $faculty = $_POST['faculty_id'];
        $prog_id = $_POST['prog_id'];
        $credit_unit = $_POST['credit_unit'];
        $level = $_POST['level'];
        $title = $_POST['title'];

        $query = $db->query("INSERT INTO `courses`(`c_code`, `semester`, `dept_id`, `faculty_id`, `prog_id`, `course_title`, `credit_unit`, `level`, `course_id`) VALUES ('$code','$semester','$dept','$faculty','$prog_id','$title','$credit_unit','$level',NULL)");

        if ($query) {
          echo '<script>alert("Course Successfully Added"); window.location="courses.php";</script>';
        } else{
          echo Error('Error');
        }

      }
    ?>
           <fieldset>
             <legend><i class="fa fa-plus"></i>Add Course</legend>
              <form action="" method="POST" role=form>
              <div class="col-lg-6">
              Course Code:
                <input type="text" name="code" class="form-control">
              </div> 
              <div class="col-lg-6">
              Course Title:
                <input type="text" name="title" class="form-control">
              </div> 
              <div class="col-lg-6">
              Faculty:
                 <select name="faculty_id" id="faculty_id" class="form-control" required>'
                      <?php 
                      $query = mysqli_query($db,"SELECT * FROM faculty");
                      while ($row = mysqli_fetch_array($query)){
                      echo "<option value='".$row['faculty_id']."'>".$row['faculty']."</option>";
                           }

                         ?>
                     </select>
                </div> 
          <div class="col-lg-6">
            Department:
        <select name="dept_id" id="dept_id" class="form-control">
                                              
        </select>
        </div> 
         <div class="col-lg-6">
          Program:
        <select name="prog_id" id="prog_id" class="form-control">
                                              
        </select>
        </div> 
              <div class="col-lg-6">
                Semester:
                <select name="semester" class="form-control">
                  <option value="1">First Semester</option>
                  <option value="2"> Second Semester</option>
                </select>
              </div>

              <div class="col-lg-6">
                Credit Unit
                <input type="text" name="credit_unit" class="form-control">
              </div>
              <div class="col-lg-6">
                Level
                <select name="level" class="form-control">
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                </select>
              </div>
              <div class="col-lg-2">
                <br>
                <input type="submit" name="submit" class="btn btn-success" value="Add">
              </div>  
              </form>
           </fieldset>
           <hr>
           <p></p>
           
        </div>
        <div class="col-lg-12">
        <fieldset>
             <legend><i class="fa fa-table"></i> List Of Courses <a href="import.php?page=courses" style="text-decoration: none; color:#ffff;" class="btn btn-success pull-right"><i class="fa fa-upload"></i> Upload Courses</a></legend>

             <table id="example2" class="table  table-responsive table-striped">
            <thead>
               <thead>
                 <tr>
                   <th>S/N</th>
                   <th>Course Code</th>
                   <th>Course Title</th>
                   <th>Credit Unit</th>
                   <th>Program</th>
                   <th>Semester</th>
                   <th>Level</th>
                   <th>Operations</th>
                 </tr>
               </thead>
               <tbody>
                 <?php
                  $page = (isset($_GET['page'])) ? $_GET['page'] : 1;

                  $limit = 5;
                  $limit_start = ($page - 1) * $limit;

                 $query = $db->query("SELECT * FROM course c INNER JOIN programs p ON c.prog_id = p.program_id INNER JOIN department d ON p.dept_id = d.dept_id INNER JOIN faculty f ON p.faculty_id = f.faculty_id   LIMIT ".$limit_start.",".$limit);
                  $no = $limit_start + 1;
                  $sn = 0;
                 while (@$row = $query->fetch_array(MYSQLI_ASSOC)) {
                  $sn++;
                   echo '<tr>';
                   echo'<td>'.$sn.'</td>';
                   echo'<td>'.$row['c_code'].'</td>';
                   echo'<td>'.$row['course_title'].'</td>';
                   echo'<td>'.$row['credit_unit'].'</td>';
                   echo'<td>'.$row['program_name'].'</td>';
                   if ($row['semester'] == 1) {
                   echo'<td>First Semester</td>';
                    }elseif ($row['semester'] == 2) {
                    echo "<td>Second Semester</td>";
                    }
                    echo'<td>'.$row['level'].'</td>';
                 echo'<td><a href="editCourse.php?id='.$row['course_id'].'" class="btn btn-default"><i class="fa fa-edit"></i></a> <a href="?id='.$row['course_id'].'" class="btn btn-default"><i class="fa fa-trash-o"></i></a></td>';
                   echo '</tr>';
                   $no++;
                 }
                 ?>

               </tbody>

             </table>
              <ul class="pagination">
            <!-- LINK FIRST AND PREV -->
            <?php
            if ($page == 1) { // Jika page adalah pake ke 1, maka disable link PREV
            ?>
                <li class="disabled"><a href="#">First</a></li>
                <li class="disabled"><a href="#">&laquo;</a></li>
            <?php
            } else { // Jika buka page ke 1
                $link_prev = ($page > 1) ? $page - 1 : 1;
            ?>
                <li><a href="courses.php?page=1">First</a></li>
                <li><a href="courses.php?page=<?php echo $link_prev; ?>">&laquo;</a></li>
            <?php
            }
            ?>

            <!-- LINK NUMBER -->
            <?php
            // Buat query untuk menghitung semua jumlah data
            $sql2 = mysqli_query($db,"SELECT COUNT(*) AS course FROM courses");
            $get_jumlah = mysqli_fetch_array($sql2);

            $jumlah_page = ceil($get_jumlah['course'] / $limit); // Hitung jumlah halamanya
            $jumlah_number = 3; // Tentukan jumlah link number sebelum dan sesudah page yang aktif
            $start_number = ($page > $jumlah_number) ? $page - $jumlah_number : 1; // Untuk awal link member
            $end_number = ($page < ($jumlah_page - $jumlah_number)) ? $page + $jumlah_number : $jumlah_page; // Untuk akhir link number

            for ($i = $start_number; $i <= $end_number; $i++) {
                $link_active = ($page == $i) ? 'class="active"' : '';
            ?>
                <li <?php echo $link_active; ?>><a href="courses.php?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
            <?php
            }
            ?>

            <!-- LINK NEXT AND LAST -->
            <?php
            // Jika page sama dengan jumlah page, maka disable link NEXT nya
            // Artinya page tersebut adalah page terakhir
            if ($page == $jumlah_page) { // Jika page terakhir
            ?>
                <li class="disabled"><a href="#">&raquo;</a></li>
                <li class="disabled"><a href="#">Last</a></li>
            <?php
            } else { // Jika bukan page terakhir
                $link_next = ($page < $jumlah_page) ? $page + 1 : $jumlah_page;
            ?>
                <li><a href="courses.php?page=<?php echo $link_next; ?>">&raquo;</a></li>
                <li><a href="courses.php?page=<?php echo $jumlah_page; ?>">Last</a></li>
            <?php
            }
            ?>
        </ul>
           </fieldset>
        </div>
      </div>
   
       </div>
       
	</div>
</div>
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(e){
  $('#faculty_id').change(function(e) {
        $.ajax({
            type:'GET',
            url:'ajaxdept.php',
            data: 'faculty_id='+$(this).val(),
            success:function(r) {
                $('#dept_id').html(r);
                //alert(r);
            }
        })
   });


   $('#dept_id').change(function(e) {
        $.ajax({
            type:'GET',
            url:'ajaxprog.php',
            data: 'dept_id='+$(this).val(),
            success:function(r) {
                $('#prog_id').html(r);
                //alert(r);
            }
        })
   });
  
});
</script>

</body>
</html>