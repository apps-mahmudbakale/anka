<?php 
session_start();
include '../inc/connect.php';
include '../inc/class.validation.php';
include '../inc/function.php';

    if (isset($_GET['id'])) {
        mysqli_query($connection,"DELETE FROM courses WHERE course_id='$_GET[id]'");
    }
 ?>

<!DOCTYPE html>
<html>
<head>
   <title>Time Table Generating System</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="shortcut icon" href="../images/udus-logo.png" />
  <link rel="stylesheet" type="text/css" href="../css/screen.css">
  <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../datatables.net-bs/css/dataTables.bootstrap.min.css">
</head>
<body>
<p></p>
<p></p>
<br>
<br>
<div id="container">
  <div class="row " style="margin-top: 70px;">
    
     <ul class="nav nav-pills pull-right">
        <li><a href="index.php" style="text-decoration: none;"><i class="fa fa-home"></i> Home</a></li>
         <li><a href="departments.php" style="text-decoration: none;"><i class="fa fa-building"></i> Departments</a></li>
        <li class="dropdown">
              <a href="#" class="dropdown-toggle" style="text-decoration: none;" data-toggle="dropdown"><i class="fa fa-cogs"></i> Time Table <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="courses.php" style="text-decoration: none;"><i class="fa fa-book"></i> Courses</a></li>
                <li><a href="venues.php" style="text-decoration: none;"><i class="fa fa-building"></i> Venues</a></li>
                <li><a href="timetable.php" style="text-decoration: none;"><i class="fa fa-table"></i> Time Table</a></li>
                 <li><a href="timetable.php" style="text-decoration: none;"><i class="fa fa-table"></i> View Generated Time Table</a></li>
              </ul>
            </li>
        <li><a href="students.php" style="text-decoration: none;"><i class="fa fa-graduation-cap"></i>View Students</a></li>
        <li><a href="view_result.php" style="text-decoration: none;"><i class="fa fa-edit"></i>Change Password</a></li>
        <li><a href="../logout.php" style="text-decoration: none;"><i class="fa fa-power-off"></i>Logout</a></li>
      </ul>
		<div class="col-lg-11 col-md-11 well" style="margin-left: 6px; min-height: 590px; width:98.666668% ;">
    <?php 

        $id = $_GET['id'];
        $query2 = $db->query("SELECT * FROM department WHERE dept_id ='$id'");
        $row = mysqli_fetch_object($query2);

      if (isset($_POST['submit'])) {
        $dept = $_POST['dept'];
        $faculty = $_POST['faculty_id'];
        $query = $db->query("UPDATE `department` SET `department`='$dept', `faculty_id` ='$faculty' WHERE `dept_id` ='$id'");

        if ($query) {
          echo '<script>alert("Department Successfully Updated"); window.location="departments.php";</script>';
        } else{
          echo Error('Error');
        }

      }
        $id = $_GET['id'];
        $query2 = $db->query("SELECT * FROM department WHERE dept_id ='$id'");
        $row = mysqli_fetch_array($query2);
    ?>
           <fieldset>
             <legend><i class="fa fa-edit"></i> Edit Department</legend>
				      <form action="" method="POST" role=form>
              <div class="col-lg-6">
              Department:
                <input type="text" name="dept" value="<?php echo $row['department'];?>" class="form-control">
              </div>
              <div class="col-lg-6">
                Faculty
                <select name="faculty_id" class="form-control">
                  <?php 
                  $query = mysqli_query($db,"SELECT * FROM faculty");

                  while ($row = mysqli_fetch_array($query)) {
                    echo "<option value='".$row['faculty_id']."'>".$row['faculty']."</option>";
                  }

                   ?>
                </select>
              </div>
              <div class="col-lg-2">
                <br>
                <input type="submit" name="submit" class="btn btn-success" value="Update Department">
              </div>  
              </form>
           </fieldset>
       </div>
       
	</div>
</div>
<script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
<script src="../bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../datatables/jquery.dataTables.min.js"></script>
<script src="../datatables/dataTables.bootstrap.min.js"></script>
<script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
</body>
</html>