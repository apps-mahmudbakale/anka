<?php 
session_start();
include '../inc/connect.php';
include '../inc/class.validation.php';
include '../inc/function.php';

    if (isset($_GET['id'])) {
        mysqli_query($connection,"DELETE FROM courses WHERE course_id='$_GET[id]'");
    }
    $page = $_GET['page'];
 ?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="shortcut icon" href="../images/udus-logo.png" />
	<link rel="stylesheet" type="text/css" href="../css/screen.css">
  <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <script src="pgcourselist.js" type="text/javascript"> </script>
</head>
<body>
<p></p>
<p></p>
<br>
<br>
<div id="container">
	<div class="row " style="margin-top: 70px;">
    
     <ul class="nav nav-pills pull-right">
        <li><a href="index.php" style="text-decoration: none;"><i class="fa fa-home"></i> Home</a></li>
         <li><a href="departments.php" style="text-decoration: none;"><i class="fa fa-building"></i> Departments</a></li>
        <li class="dropdown">
              <a href="#" class="dropdown-toggle" style="text-decoration: none;" data-toggle="dropdown"><i class="fa fa-cogs"></i> Time Table <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="courses.php" style="text-decoration: none;"><i class="fa fa-book"></i> Courses</a></li>
                <li><a href="venues.php" style="text-decoration: none;"><i class="fa fa-building"></i> Venues</a></li>
                <li><a href="timetable.php" style="text-decoration: none;"><i class="fa fa-table"></i> Time Table</a></li>
                 <li><a href="timetable.php" style="text-decoration: none;"><i class="fa fa-table"></i> View Generated Time Table</a></li>
              </ul>
            </li>
        <li><a href="students.php" style="text-decoration: none;"><i class="fa fa-graduation-cap"></i>View Students</a></li>
        <li><a href="view_result.php" style="text-decoration: none;"><i class="fa fa-edit"></i>Change Password</a></li>
        <li><a href="../logout.php" style="text-decoration: none;"><i class="fa fa-power-off"></i>Logout</a></li>
      </ul>
		<div class="col-lg-11 col-md-11 well" style="margin-left: 6px; min-height: 590px; width:98.666668% ;">
    
           <fieldset>
             <legend><i class="fa fa-upload"></i> UPLOAD <?php echo strtoupper($page); ?></legend>
				<?php 
					switch ($page) {
						case 'courses':
							if (isset($_POST['upload'])) {
								 $filename=$_FILES["file"]["tmp_name"];	

								 if($_FILES["file"]["size"] > 0)
								 {
								  	$file = fopen($filename, "r");
							        while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
							         {
							         	$c_code = $getData[1];
							         	$semester = $getData[2];
							         	$title = $getData[3];
							         	$cunit = $getData[4];
							         	$level = $getData[5];
							         	$query = mysqli_query($db,"SELECT * FROM programs WHERE program_name='".$getData[0]."'");
										$row = mysqli_fetch_array($query);
							         	$prog_id = $row['program_id'];
							         	//print_r($row);
							         	if (mysqli_num_rows(mysqli_query($db,"SELECT * FROM course WHERE c_code='".$getData[1]."'")) > 0) {
							         		
							         	}else{
							         		 $sql = "INSERT INTO `course`(`c_code`, `semester`, `prog_id`, `course_title`, `credit_unit`, `level`, `course_id`) VALUES ('$c_code','$semester','$prog_id','$title','$cunit','$level',NULL)";
							           $result = mysqli_query($db, $sql);
									    // var_dump(mysqli_error_list($con));
									    // exit();
										if(!isset($result))
										{
											echo "<script type=\"text/javascript\">
													alert(\"Invalid File:Please Upload CSV File.\");
													window.location = \"courses.php\"
												  </script>";		
										}
										else {
											  echo "<script type=\"text/javascript\">
												alert(\"CSV File has been successfully Imported.\");
												window.location = \"courses.php\"
											</script>";
										}
							         	}
							          
							         }
									
							         fclose($file);	
								 }
							}
						?>
							<form action="" method="POST" enctype="Multipart/form-data">
								<div class="col-lg-6">
									<input type="file" name="file" class="form-control">
								</div>
								<div class="col-lg-6">
									<input type="submit" name="upload" value="Upload" class="btn btn-success">
								</div>
							</form>
						<?php	break;
						case 'department':
								if (isset($_POST['upload'])) {
								 $filename=$_FILES["file"]["tmp_name"];	

								 if($_FILES["file"]["size"] > 0)
								 {
								  	$file = fopen($filename, "r");
							        while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
							         {
							         
							         	//print_r($row);
							           $sql = "INSERT INTO `department`(`dept_id`, `department`) VALUES (NULL,'".$getData[0]."')";
							           $result = mysqli_query($db, $sql);
									    // var_dump(mysqli_error_list($con));
									    // exit();
										if(!isset($result))
										{
											echo "<script type=\"text/javascript\">
													alert(\"Invalid File:Please Upload CSV File.\");
													window.location = \"departments.php\"
												  </script>";		
										}
										else {
											  echo "<script type=\"text/javascript\">
												alert(\"CSV File has been successfully Imported.\");
												window.location = \"departments.php\"
											</script>";
										}
							         }
									
							         fclose($file);	
								 }
							}
						?>
							<form action="" method="POST" enctype="Multipart/form-data">
								<div class="col-lg-6">
									<input type="file" name="file" class="form-control">
								</div>
								<div class="col-lg-6">
									<input type="submit" name="upload" value="Upload" class="btn btn-success">
								</div>
							</form>
							<?php break;
							case 'venues':
							if (isset($_POST['upload'])) {
								 $filename=$_FILES["file"]["tmp_name"];	

								 if($_FILES["file"]["size"] > 0)
								 {
								  	$file = fopen($filename, "r");
							        while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
							         {
							         
							         	//print_r($row);
							           $sql = "INSERT INTO `venue`(`venue_id`, `venue`, `venue_title`) VALUES (NULL,'".$getData[0]."','".$getData[1]."')";
							           $result = mysqli_query($db, $sql);
									    // var_dump(mysqli_error_list($con));
									    // exit();
										if(!isset($result))
										{
											echo "<script type=\"text/javascript\">
													alert(\"Invalid File:Please Upload CSV File.\");
													window.location = \"venues.php\"
												  </script>";		
										}
										else {
											  echo "<script type=\"text/javascript\">
												alert(\"CSV File has been successfully Imported.\");
												window.location = \"venues.php\"
											</script>";
										}
							         }
									
							         fclose($file);	
								 }
							}
						?>
							<form action="" method="POST" enctype="Multipart/form-data">
								<div class="col-lg-6">
									<input type="file" name="file" class="form-control">
								</div>
								<div class="col-lg-6">
									<input type="submit" name="upload" value="Upload" class="btn btn-success">
								</div>
							</form>
								<?php break;
						default:
							# code...
							break;
					}


					 ?>

           </fieldset>
       </div>
       
	</div>
</div>
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script src="../js/jQuery-1.10.0.js"></script>
<script src="../datatables/jquery.dataTables.min.js"></script>
<script src="../datatables/dataTables.bootstrap.min.js"></script>
<script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>

</body>
</html>


