<?php 
session_start();
include '../inc/connect.php';
$query = mysqli_query($db,"SELECT * FROM reg_courses");
 ?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css"/>
<link rel="shortcut icon" href="../images/udus-logo.png"/>
</head>
<body>
 <div class="col-lg-offset-1  col-lg-10 col-md-11 col-md-offset-1 col-sm-12 col-xs-12 ">
      <div class='col-lg-1'></div>
      <div class="col-lg-2"> 
           <img src="../images/udus-logo.png" width='80' height='80' style='float:right'>
      </div>
      <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12" style='color:#000'>
         <h2>USMANU DANFODIYO UNIVERSITY,SOKOTO </h2>
         <h4 class=' text-center col-lg-9'></h4>
      </div>
      
  </div>
    <div class="col-lg-offset-1 col-lg-10 col-md-10 col-md-offset-1 col-sm-12 col-xs-12 thumbnail" 
      style="padding:5px;">
      <table class="table table-striped">
        <th>S/N</th>
        <th>ADM NO</th>
        <th>COURSE CODE</th>
        <th>LEVEL</th>
        <th>SESSION</th>
        <?php 
        $sn=0;
        while ($row = mysqli_fetch_array($query)) {
          $sn++;
          echo " <tr>
          <td>".$sn."</td>
          <td>".$row['adm_no']."</td>
          <td>".$row['c_code']."</td>
          <td>".$row['level']."</td>
          <td>".$row['session']."</td>
        </tr>";
        }

         ?>
      </table>
    </div>
</body>
</html>

 