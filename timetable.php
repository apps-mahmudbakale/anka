<?php 
session_start();
include '../inc/connect.php';
include '../inc/class.validation.php';
include '../inc/function.php';
 ?>
 
<!DOCTYPE html>
<html>
<head>
   <title>Time Table Generating System</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="shortcut icon" href="../images/udus-logo.png" />
  <link rel="stylesheet" type="text/css" href="../css/screen.css">
  <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../datatables.net-bs/css/dataTables.bootstrap.min.css">
</head>
<body>
<p></p>
<p></p>
<br>
<br>
<div id="container">
  <div class="row " style="margin-top: 70px;">
    
    <ul class="nav nav-pills ">
        <li><a href="index.php" style="text-decoration: none;"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="changepass.php" style="text-decoration: none;"><i class="fa fa-edit"></i> Change Password</a></li>
      <li><a href="../logout.php" style="text-decoration: none;"><i class="fa fa-power-off"></i> Logout</a></li>
      </ul>
    <div class="col-lg-11 col-md-11 well" style="margin-left: 6px; min-height: 590px; width:98.666668% ;">
        <div class="col-lg-12">
          <?php 
          if (isset($_POST['submit'])) {
            $weeks = (int)$_POST['week'];
            $session = $_POST['session'];
            $days = array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
            $times = array('08:30-11:00','12:00-02:00','03:00-05:00');

            $date = date('d/m/Y');
            $date;
            for ($wk=0; $wk<=$weeks; $wk++){ 
              //echo $i;
              $daysNumber = 0;
              for ($dy=0; $dy < count($days); $dy++) {
                $day = $days[$dy];
                
                //$date = date('d/m/Y');

                // for($tm = 0; $tm < count($times); $tm++){
                  // $time = $times[$tm];


                  $courses = mysqli_query($db,"SELECT c_code, COUNT(*) AS cnt FROM reg_courses WHERE c_session ='$session' AND considered ='false' GROUP BY c_code");
                  $tm = 0;
                  // echo mysqli_num_rows($courses);
                  while( $row = mysqli_fetch_assoc($courses) && $tm < count($times)) {
                     $time = $times[$tm];
                     $c_code = $row['c_code'];
                     $count = $row['cnt'];
                    
                     $v = mysqli_query($db,"SELECT venue_id, venue_title FROM venue WHERE exam_capacity >='$count' AND full ='false' limit 1");
                     // echo mysqli_num_rows($v) . "<br>";
                    
                    if (!empty(mysqli_num_rows($v))) {
                      $vrow = mysqli_fetch_assoc($v);
                      $venue_id = $vrow['venue_id'];
                      //$c_code = $row_courses['c_code'];
                      $exam_row = mysqli_query($db,"SELECT COUNT(*) AS cnt FROM exam WHERE venue_id ='$venue_id' AND mdate = '$date' ");
                      $exam_cnt = mysqli_fetch_assoc($exam_row)['cnt'];
                      // echo $exam_cnt . "<br>";
                      //$row_space = mysqli_fetch_array($space_cnt);
                      if(mysqli_query($db,"INSERT INTO `exam`(`venue_id`, `exam_time`, `c_code`,`mday`, `mdate`) VALUES ('$venue_id','$time','$c_code','$day','$date')")){
                        
                          $exam_cnt++;
                          mysqli_query($db, "UPDATE reg_courses SET considered = 'true' WHERE c_code = '$c_code'");
                          $tm++;
                          if ($exam_cnt == 3) {
                                mysqli_query($db,"UPDATE `venue` SET `full`='true' WHERE  venue_id ='$venue_id'");
                                break;                        
                          } else{
                           // echo "No space For".$c_code;
                            mysqli_query($db,"UPDATE reg_courses  SET considered ='cant' WHERE c_code = '$c_code'");
                          }
                      }
                      


                    }//ifempty

                  } //forWhileLoop
                  $daysNumber++;
                  $date = date("d/m/Y", strtotime("now +$daysNumber day"  ));
                  
                 }//forT
              }   
              echo "<script>alert('Generated Successfully'); window.location='index.php';</script>";
             
            }


           ?>
          <fieldset>
             <legend><i class="fa fa-random"></i> Generate Time Table</legend>
              <form action="" method="POST" role=form>
              <div class="col-lg-12">
              Week(s):
              <input type="Number" name="week" class="form-control">
              </div>
            <div class="col-lg-12">
                Session:
                <select name="session" class="form-control">
                  <?php 
                         for($i=2014; $i<date('Y');$i++) {
                             $session = $i."/".($i+1);
                             echo "<option>".$session."</option>";
                         }
                      ?>
                </select>
              </div>
              <div class="col-lg-8">
                <br>
                <input type="submit" name="submit" class="btn btn-success" value="OK">
              </div>  
              </form>
           </fieldset>
      </div>
</div>
</div>
</div>
<script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
<script src="../bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../datatables/jquery.dataTables.min.js"></script>
<script src="../datatables/dataTables.bootstrap.min.js"></script>
<script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>

</body>
</html>