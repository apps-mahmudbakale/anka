<?php 
session_start();
include '../inc/connect.php';

$id = $_GET['std'];

$res = mysqli_query($db,"SELECT * FROM students s  INNER JOIN programs p ON s.prog_id = p.program_id  WHERE s.student_id ='$id'");
$std = mysqli_fetch_array($res);

$query = mysqli_query($db,"SELECT * FROM my_courses m INNER JOIN courses c ON m.course_id = c.course_id WHERE m.student_id ='$id'");
 ?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css"/>
<link rel="shortcut icon" href="../images/udus-logo.png"/>
</head>
<body>
 <div class="col-lg-offset-1  col-lg-10 col-md-11 col-md-offset-1 col-sm-12 col-xs-12 ">
      <div class='col-lg-1'></div>
      <div class="col-lg-2"> 
           <img src="../images/udus-logo.png" width='80' height='80' style='float:right'>
      </div>
      <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12" style='color:#000'>
         <h2>USMANU DANFODIYO UNIVERSITY,SOKOTO </h2>
         <h4 class=' text-center col-lg-9'></h4>
      </div>
      
  </div>
    <div class="col-lg-offset-1 col-lg-10 col-md-10 col-md-offset-1 col-sm-12 col-xs-12 thumbnail" 
      style="padding:5px;">
      <table class="table table-striped">
        <tr>
          <td>Name: <?php echo strtoupper($std['surname'])." ".$std['firstname']; ?></td>
          <td>Admission No: <?php echo @$std['matric_no']; ?> </td>
          <td>Program: <?php echo @$std['program_name']; ?></td>
          <td>Level: <?php echo "UG ".@$std['level']; ?></td>

        </tr>
        <th>S/N</th>
        <th>Course Code</th>
        <th>Course Title</th>
        <th>Credit Unit</th>
        <?php 
        $sn=0;
        while ($row = mysqli_fetch_array($query)) {
          $sn++;
          echo " <tr>
          <td>".$sn."</td>
          <td>".$row['course_code']."</td>
          <td>".$row['course_title']."</td>
          <td>".$row['credit_unit']."</td>
        </tr>";
        }

         ?>
      </table>
    </div>
</body>
</html>

 