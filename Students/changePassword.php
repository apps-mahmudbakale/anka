<?php 
session_start();
include '../inc/connect.php';
include '../inc/class.validation.php';
include '../inc/function.php';


 ?>

<!DOCTYPE html>
<html>
<head>
	<title>Time Table Generating System</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="shortcut icon" href="../images/udus-logo.png" />
	<link rel="stylesheet" type="text/css" href="../css/screen.css">
  <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <script src="pgcourselist.js" type="text/javascript"> </script>
</head>
<body>
<p></p>
<p></p>
<br>
<br>
<div id="container">
	<div class="row " style="margin-top: 70px;">
    
     <ul class="nav nav-pills pull-right">
        <li><a href="index.php" style="text-decoration: none;"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="timetable.php" style="text-decoration: none;"><i class="fa fa-table"></i> General Time Table</a></li>
        <li><a href="changePassword.php" style="text-decoration: none;"><i class="fa fa-edit"></i> Change Password</a></li>
        <li><a href="../logout.php" style="text-decoration: none;"><i class="fa fa-power-off"></i> Logout</a></li>
      </ul>
		<div class="col-lg-11 col-md-11 well" style="margin-left: 6px; min-height: 590px; width:98.666668% ;">
           <fieldset>
             <legend><h3>Change Password</h3></legend>
             <?php
             if(isset($_POST['change'])) {
                    $currentPassword = addslashes($_POST['cpassword']);
                    $newPassword = addslashes($_POST['password']);
                    $confirmPassword = addslashes($_POST['password1']);

                  $query = $db->query("SELECT * FROM students WHERE matric_no='$_SESSION[student]' AND password = MD5('$currentPassword')");

                  //echo "SELECT * FROM students WHERE admission_number='$_SESSION[student]' AND password = MD5('$currentPassword')";

                  if($query->num_rows  == 1 ) {
                       
                       if($confirmPassword == $newPassword) {
                           $query = $db->query("UPDATE students SET  password = MD5('$newPassword')  WHERE matric_no='$_SESSION[student]'");
                           echo "<script>alert('Password changed');window.location='../logout.php'</script>";
                       } else {
                          echo "<script>alert('Password Mismatch')</script>";
                       }

                  } else {
                    echo "<script>alert('Wrong Current Password')</script>";
                  }
             }
             ?>
				     <form method="post">
                  <table class="table">
                    <tr>
                       <td class="col-lg-2">Current Password:</td>
                       <td><input type="password" name="cpassword" class="form-control"/></td>
                    </tr>
                     <tr>
                       <td class="col-lg-2">New Password:</td>
                       <td><input type="password" name="password" class="form-control"/></td>
                    </tr>
                     <tr>
                       <td class="col-lg-2">Confirm Password:</td>
                       <td><input type="password" name="password1" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td>
                            <button name='change' class="btn btn-success">Change Password</button>
                        </td>
                    </tr>
                  </table>
             </form>
           </fieldset>
       </div>
       
	</div>
</div>
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript">

/*
  $(function(){
    $('#unit').change(AddProgram);
    AddProgram();
  });

function AddProgram(){
  var entry = $('#unit option:selected').text();
  if (entry == 'Computer') {
    //var o = new Option("MPhil.Computer", "MPhil.Computer");
    //$(o).html("MPhil.Computer");
    //$('#course').append(o);
    //addOption(document.course,"MPhil.Statistics");
    //$('#course').append('<option>MPhil.Statistics</option>');
  } else if(entry == 'Mathematics'){
    var o = new Option("MPhil.Mathematics", "MPhil.Mathematics");
    $(o).html("MPhil.Mathematics");
    $('#course').append(o);
  }

}
*/

</script>

</body>
</html>