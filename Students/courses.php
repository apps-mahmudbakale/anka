<?php 
session_start();
include '../inc/connect.php';
include '../inc/class.validation.php';
include '../inc/function.php';
if (!empty($_SESSION['student'])) {
   $student = $_SESSION['student'];
   $s = $db->query("SELECT * FROM students WHERE matric_no = '$student'");
   $ss = mysqli_fetch_array($s);
   $student_id = $ss['student_id'];
   $program = $ss['dept_id'];
   $factulty = $ss['factulty_id'];
  $query2 = $db->query("SELECT * FROM students WHERE student_id = '$student_id'");
  $row2 = mysqli_fetch_array($query2);
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Time Table Generating System</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="shortcut icon" href="../images/udus-logo.png" />
	<link rel="stylesheet" type="text/css" href="../css/screen.css">
  <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
  <script src="pgcourselist.js" type="text/javascript"> </script>
</head>
<body>
<p></p>
<p></p>
<br>
<br>
<div id="container">
	<div class="row " style="margin-top: 70px;">
    
     <ul class="nav nav-pills pull-right">
        <li><a href="index.php" style="text-decoration: none;"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="courses.php" style="text-decoration: none;"><i class="fa fa-pencil"></i> Register Courses</a></li>
        <li><a href="timetable.php" style="text-decoration: none;"><i class="fa fa-table"></i> General Time Table</a></li>
        <li><a href="changePassword.php" style="text-decoration: none;"><i class="fa fa-edit"></i> Change Password</a></li>
        <li><a href="../logout.php" style="text-decoration: none;"><i class="fa fa-power-off"></i> Logout</a></li>
      </ul>
		<div class="col-lg-11 col-md-11 well" style="margin-left: 6px; min-height: 590px; width:98.666668% ;">
<?php
if(isset($_POST['button'])) {
      $co_ids = isset($_POST['co_ids']) ? $_POST['co_ids'] : array();
      $units = 0;
      $session = $_POST['session'];
      $x = 0;
      foreach($co_ids as $co_id) {
         $chk = mysqli_query($db,"SELECT credit_unit FROM courses WHERE course_id = $co_id GROUP BY course_code");
        $row = $chk->fetch_array(MYSQLI_ASSOC);
        $units += $row['credit_unit'];
      }
      foreach($co_ids as $co_id){
        if($units >= $_POST['min_unit']) {
            $get = mysqli_query($db,"SELECT * FROM courses WHERE course_id ='$co_id'");
            $getrow = mysqli_fetch_array($get);
            $code = $getrow['course_code'];
            $title = $getrow['course_title'];
            $unit = $getrow['credit_unit'];
            $level = $getrow['level'];
            //print_r($getrow);
             mysqli_query($db,"INSERT INTO `my_courses`(`id`, `student_id`, `course_id`,`session`) VALUES (NULL,'$student_id','$co_id', '$session')");
             mysqli_query($db,"INSERT INTO `reg_courses`(`reg_id`, `adm_no`, `course_code`, `course_title`, `credit_unit`, `level`, `session`) VALUES (NULL,'$student','$code','$title','$unit','$level','$session')");
        }else {
          $x++;
        }
      }
      if($x > 0) {
        error('Minimum units not reached or maximum units exceeded');
      }
}
?>

<form action="" method="POST">
  <fieldset>
    <legend><i class="fa fa-pencil"></i> Register Courses</legend>
    <table class="table table-striped">
      <tr>
        <td>Name:</td>
        <td><?php echo strtoupper($row2['surname'])." ".strtoupper($row2['firstname']) ?></td>
      </tr>
      <tr>
        <td>Adm No:</td>
        <td><?php echo $row2['matric_no']; ?></td>
      </tr>
    </table>
    <div class="col-lg-6">
              Faculty:
                 <select name="faculty_id" id="faculty_id" class="form-control" required>'
                      <?php 
                      $query = mysqli_query($db,"SELECT * FROM faculty");
                      while ($row = mysqli_fetch_array($query)){
                      echo "<option value='".$row['faculty_id']."'>".$row['faculty']."</option>";
                           }

                         ?>
                     </select>
                </div> 
          <div class="col-lg-6">
            Department:
        <select name="dept_id" id="dept_id" class="form-control">
                                              
        </select>
        </div> 
         <div class="col-lg-6">
          Program:
        <select name="prog_id" id="prog_id" class="form-control">
                                              
        </select>
        </div> 
        <div class="col-lg-6">
          Level
          <select name="level" class="form-control">
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
          </select>
        </div>
        <div class="col-lg-6">
           Session:
                <select name="session" class="form-control">
                  <?php 
                         for($i=2014; $i<date('Y');$i++) {
                             $session = $i."/".($i+1);
                             echo "<option>".$session."</option>";
                         }
                      ?>
                </select>
        </div>
        <div class="col-lg-6">
          <br>
          <input type="submit" name="find" value="Find" class="btn btn-success">
        </div>
  </fieldset>
</form>
<?php 
if (isset($_POST['find'])) {
  $faculty = $_POST['faculty_id'];
  $dept = $_POST['dept_id'];
  $prog = $_POST['prog_id'];
  $level = $_POST['level'];
  $session = $_POST['session'];


  $query = mysqli_query($db,"SELECT * FROM courses WHERE faculty_id = '$faculty' AND dept_id ='$dept' AND prog_id  ='$prog' AND level = '$level'");?>
      <form action="" method="POST">
  <table class="table table-striped">
  <thead>
    <th>S/N</th>
    <th></th>
    <th>Course Code</th>
    <th>Course Title</th>
    <th>Credit Units(s)</th>
   
  </thead>
  <tbody>
    <?php
      $max_unit = 0;
      while($row = mysqli_fetch_array($query)) {
       $sn++;
        $max_unit = 48;
        $min_unit = 4;
        echo "\t<input type='hidden' name='session' value='$session'>\n";
        echo "\t<input type='hidden' name='min_unit' value='$min_unit'>\n";
        echo "\t<input type='hidden' name='max_unit' value='$max_unit'>\n";
        echo "\t<tr ";
        echo ($sn % 2 == 0) ? 'bgcolor="#F4F4F4"' : '';
        echo " >\n";
          echo "\t\t<td>$sn</td>\n";
        echo "\t\t<td><input  type='checkbox' id='checkbox-1-$sn' class='regular-checkbox' name='co_ids[]' value='$row[course_id]' ";
        echo "/><label for='checkbox-1-$sn'></label></td>\n";
        echo "\t\t<td>".$row['course_code']."</td>\n";
        echo "\t\t<td>".$row['course_title']."</td>\n";
        echo "\t\t<td>".$row['credit_unit']."</td>\n";   
        echo "\t</tr>\n";
      }
    ?>
  </tbody>
</table>
<hr>
<table class="table table-striped" width="100%" cellpadding="5">
       <tr bgcolor="#F4F4F4">
         <td width="87%"><strong>Selected units:</strong></td>
         <td width="13%" id='units'>0</td>
       </tr>
       <tr>
         <td><strong>Required units:</strong></td>
         <td><?php echo @$min_unit ?></td>
       </tr>
       <tr bgcolor="#F4F4F4">
         <td><strong>Maximum units:</strong></td>
         <td><?php  echo @$max_unit ?></td>
       </tr>
    </table>
    <hr>
<input type="submit" name="button" class="btn btn-info" value="Register" />
</form>

<?php
}



 ?>
      <br>
       <a href="print_courses.php?std=<?php echo $student_id ?>"  target='blank' class="btn btn-primary">Registered Courses</a>
       </div>
       
	</div>
</div>
<script src="../js/jquery-1.10.0.js"></script>
<script src="../js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
<script src="../bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(e){
  $('#faculty_id').change(function(e) {
        $.ajax({
            type:'GET',
            url:'ajaxdept.php',
            data: 'faculty_id='+$(this).val(),
            success:function(r) {
                $('#dept_id').html(r);
                //alert(r);
            }
        })
   });


   $('#dept_id').change(function(e) {
        $.ajax({
            type:'GET',
            url:'ajaxprog.php',
            data: 'dept_id='+$(this).val(),
            success:function(r) {
                $('#prog_id').html(r);
                //alert(r);
            }
        })
   });
  
});
</script>
</body>
</html>