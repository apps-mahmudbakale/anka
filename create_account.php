<?php 
session_start();
include 'inc/connect.php';
include 'inc/class.validation.php';
include 'inc/function.php';


 ?>

<!DOCTYPE html>
<html>
<head>
	<title>Time Table Generating System</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="shortcut icon" href="images/udus-logo.png" />
	<link rel="stylesheet" type="text/css" href="css/screen.css">
  <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <script src="pgcourselist.js" type="text/javascript"> </script>
</head>
<body>
<p></p>
<p></p>
<br>
<br>
<div id="container">
	<div class="row " style="margin-top: 70px;">
    
      <ul class="nav nav-pills pull-right">
        <li><a href="index.php" style="text-decoration: none;"><i class="fa fa-home"></i> Home</a></li>
      </ul>
		<div class="col-lg-11 col-md-11 well" style="margin-left: 6px; min-height: 590px; width:98.666668% ;">
           <fieldset>
             <legend><img src="images/create account.png"></legend>

             <?php
             if (isset($_POST['submit'])) {
               $number = ($_POST['number']);
               $password = (md5($_POST['number']));
               $surname = ($_POST['surname']);
               $firstname = ($_POST['firstname']);
               $othername = ($_POST['oname']);
               $phone = ($_POST['phone']);
               $prog_id = $_POST['prog_id'];
               $level   = $_POST['level'];

               $query = $db->query("INSERT INTO `students`(`student_id`, `matric_no`, `firstname`, `surname`, `othername`, `phone`, `prog_id`, `level`, `password`) VALUES (NULL,'$number','$firstname','$surname','$othername','$phone','$prog_id', '$level','$password')");

               if ($query) {
                 echo success('Registration Successful');
               } else{
                echo error('Registration Failed');
               }


             }


             ?>

               <form action="" method="POST">
                                        <div class="col-lg-6">
                                            Admission Number:
                                            <input type="number" name="number"  class="form-control"/ required>
                                        </div>
                                        <div class="col-lg-6">
                                            Surname:
                                            <input type="text" name="surname"    class="form-control"/ required>
                                        </div>

                                        <div class="col-lg-6">
                                            First Name:
                                            <input type="text" name="firstname"  class="form-control"/ required>
                                        </div>

                                        <div class="col-lg-6">
                                            Other Name:
                                            <input type="text" name='oname' class="form-control">
                                        </div>
                                         <div class="col-lg-6">
                                            Faculty:
                                            <select name="faculty_id" id="faculty_id" class="form-control" required>'
                                              <?php 
                                                $query = mysqli_query($db,"SELECT * FROM faculty");
                                                while ($row = mysqli_fetch_array($query)){
                                                    echo "<option value='".$row['faculty_id']."'>".$row['faculty']."</option>";
                                                }

                                                ?>
                                            </select>
                                        </div> 
                                        <div class="col-lg-6">
                                            Department:
                                            <select name="dept_id" id="dept_id" class="form-control">
                                              
                                            </select>
                                        </div> 
                                         <div class="col-lg-6">
                                           Program:
                                            <select name="prog_id" id="prog_id" class="form-control" required>'
                                            </select>
                                        </div> 
                                        
                                          <div class="col-lg-6">
                                            Level
                                            <select name="level" class="form-control">
                                              <option>1</option>
                                              <option>2</option>
                                              <option>3</option>
                                              <option>4</option>
                                            </select>
                                          </div>
                                      <div class="col-lg-6">
                                        Phone Number:
                                         <input type="number" required name="phone" class="form-control">
                                        </div>
                                         <div class="col-lg-6">
                                        Password:
                                         <input type="password" required name="password" class="form-control">
                                        </div>
                                        <div class="col-lg-6">
                                          <br>
                                            <button type="submit" name="submit" class="btn btn-success"><i class="fa fa-check-circle"></i> Register </button>
                                        </div>

           </fieldset>
       </div>
       
	</div>
</div>
<script src="js/jquery-1.10.0.js"></script>
<script src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script src="bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(e){
  $('#faculty_id').change(function(e) {
        $.ajax({
            type:'GET',
            url:'admin/ajaxdept.php',
            data: 'faculty_id='+$(this).val(),
            success:function(r) {
                $('#dept_id').html(r);
                //alert(r);
            }
        })
   });


   $('#dept_id').change(function(e) {
        $.ajax({
            type:'GET',
            url:'admin/ajaxprog.php',
            data: 'dept_id='+$(this).val(),
            success:function(r) {
                $('#prog_id').html(r);
                //alert(r);
            }
        })
   });
  
});
</script>

</body>
</html>